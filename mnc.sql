-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: mnc
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'pakaian','Pakaian',NULL,'2021-09-25 10:05:12','2021-09-25 10:05:12'),(2,'topi','Topi',NULL,'2021-09-25 10:05:12','2021-09-25 10:05:12'),(3,'celana','Celana',NULL,'2021-09-25 10:05:12','2021-09-25 10:05:12'),(4,'jas','Jas',NULL,'2021-09-25 10:05:12','2021-09-25 10:05:12'),(5,'sepatu','Sepatu',NULL,'2021-09-25 10:05:12','2021-09-25 10:05:12'),(6,'jaket','Jaket',NULL,'2021-09-25 10:05:12','2021-09-25 10:05:12'),(7,'tas','Tas',NULL,'2021-09-25 10:05:12','2021-09-25 10:05:12'),(8,'dompet','Dompet',NULL,'2021-09-25 10:05:12','2021-09-25 10:05:12');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2021_09_25_072455_create_category_table',1),(3,'2021_09_25_072504_create_product_table',1),(4,'2021_09_25_072538_create_product_gambar_table',1),(5,'2021_09_25_072546_create_product_warna_table',1),(6,'2021_09_25_072552_create_product_ukuran_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci,
  `base_price` double(20,2) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (6,'tester-pakaian',1,'Topi Tidur2','Topi untuk tidur',90000.00,1,NULL,'2021-09-25 13:16:26','2021-09-25 13:16:26'),(7,'baju-tidur3',1,'Topi Tidur3','Topi untuk tidur',90000.00,1,NULL,'2021-09-25 13:16:33','2021-09-25 13:16:33'),(8,'baju-tidur4',1,'Topi Tidur4','Topi untuk tidur',90000.00,1,NULL,'2021-09-25 13:16:36','2021-09-25 13:16:36'),(9,'baju-tidur5',1,'Topi Tidur5','Topi untuk tidur',90000.00,1,NULL,'2021-09-25 13:16:40','2021-09-25 13:16:40'),(10,'baju-tidur6',1,'Topi Tidur6','Topi untuk tidur',90000.00,1,NULL,'2021-09-25 13:16:43','2021-09-25 13:16:43'),(11,'baju-tidur7',1,'Topi Tidur7','Topi untuk tidur',90000.00,1,NULL,'2021-09-25 13:16:47','2021-09-25 13:16:47'),(24,'tester-pakaian2',1,'tester dong cuuk','tester',90000.00,1,NULL,'2021-09-25 19:30:26','2021-09-25 19:30:26'),(26,'breezeee',1,'pneumatic','tester sillicon',90000.00,1,NULL,'2021-09-25 19:31:53','2021-09-25 19:31:53');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_gambar`
--

DROP TABLE IF EXISTS `product_gambar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_gambar` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_gambar`
--

LOCK TABLES `product_gambar` WRITE;
/*!40000 ALTER TABLE `product_gambar` DISABLE KEYS */;
INSERT INTO `product_gambar` VALUES (5,6,'https://www.google.com','2021-09-25 13:16:26','2021-09-25 13:16:26'),(6,6,'https://www.facebook.com','2021-09-25 13:16:26','2021-09-25 13:16:26'),(7,7,'https://www.google.com','2021-09-25 13:16:33','2021-09-25 13:16:33'),(8,7,'https://www.facebook.com','2021-09-25 13:16:33','2021-09-25 13:16:33'),(9,8,'https://www.google.com','2021-09-25 13:16:36','2021-09-25 13:16:36'),(10,8,'https://www.facebook.com','2021-09-25 13:16:36','2021-09-25 13:16:36'),(11,9,'https://www.google.com','2021-09-25 13:16:40','2021-09-25 13:16:40'),(12,9,'https://www.facebook.com','2021-09-25 13:16:40','2021-09-25 13:16:40'),(13,10,'https://www.google.com','2021-09-25 13:16:43','2021-09-25 13:16:43'),(14,10,'https://www.facebook.com','2021-09-25 13:16:43','2021-09-25 13:16:43'),(15,11,'https://www.google.com','2021-09-25 13:16:47','2021-09-25 13:16:47'),(16,11,'https://www.facebook.com','2021-09-25 13:16:47','2021-09-25 13:16:47'),(27,24,'https://www.google.com','2021-09-25 19:30:26','2021-09-25 19:30:26'),(28,24,'https://www.facebook.com','2021-09-25 19:30:26','2021-09-25 19:30:26'),(29,26,'https://www.google.com','2021-09-25 19:31:53','2021-09-25 19:31:53'),(30,26,'https://www.facebook.com','2021-09-25 19:31:53','2021-09-25 19:31:53');
/*!40000 ALTER TABLE `product_gambar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_ukuran`
--

DROP TABLE IF EXISTS `product_ukuran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_ukuran` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(20,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_ukuran`
--

LOCK TABLES `product_ukuran` WRITE;
/*!40000 ALTER TABLE `product_ukuran` DISABLE KEYS */;
INSERT INTO `product_ukuran` VALUES (7,6,'XL',500000.00,'2021-09-25 13:16:26','2021-09-25 13:16:26'),(8,6,'L',450000.00,'2021-09-25 13:16:26','2021-09-25 13:16:26'),(9,6,'M',400000.00,'2021-09-25 13:16:26','2021-09-25 13:16:26'),(10,7,'XL',500000.00,'2021-09-25 13:16:33','2021-09-25 13:16:33'),(11,7,'L',450000.00,'2021-09-25 13:16:33','2021-09-25 13:16:33'),(12,7,'M',400000.00,'2021-09-25 13:16:33','2021-09-25 13:16:33'),(13,8,'XL',500000.00,'2021-09-25 13:16:36','2021-09-25 13:16:36'),(14,8,'L',450000.00,'2021-09-25 13:16:36','2021-09-25 13:16:36'),(15,8,'M',400000.00,'2021-09-25 13:16:36','2021-09-25 13:16:36'),(16,9,'XL',500000.00,'2021-09-25 13:16:40','2021-09-25 13:16:40'),(17,9,'L',450000.00,'2021-09-25 13:16:40','2021-09-25 13:16:40'),(18,9,'M',400000.00,'2021-09-25 13:16:40','2021-09-25 13:16:40'),(19,10,'XL',500000.00,'2021-09-25 13:16:43','2021-09-25 13:16:43'),(20,10,'L',450000.00,'2021-09-25 13:16:43','2021-09-25 13:16:43'),(21,10,'M',400000.00,'2021-09-25 13:16:43','2021-09-25 13:16:43'),(22,11,'XL',500000.00,'2021-09-25 13:16:47','2021-09-25 13:16:47'),(23,11,'L',450000.00,'2021-09-25 13:16:47','2021-09-25 13:16:47'),(24,11,'M',400000.00,'2021-09-25 13:16:47','2021-09-25 13:16:47'),(40,24,'XL',90000.00,'2021-09-25 19:30:27','2021-09-25 19:30:27'),(41,24,'L',85000.00,'2021-09-25 19:30:27','2021-09-25 19:30:27'),(42,24,'M',80000.00,'2021-09-25 19:30:27','2021-09-25 19:30:27'),(43,26,'XL',90000.00,'2021-09-25 19:31:53','2021-09-25 19:31:53'),(44,26,'L',85000.00,'2021-09-25 19:31:53','2021-09-25 19:31:53'),(45,26,'M',80000.00,'2021-09-25 19:31:53','2021-09-25 19:31:53');
/*!40000 ALTER TABLE `product_ukuran` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_warna`
--

DROP TABLE IF EXISTS `product_warna`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_warna` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_warna`
--

LOCK TABLES `product_warna` WRITE;
/*!40000 ALTER TABLE `product_warna` DISABLE KEYS */;
INSERT INTO `product_warna` VALUES (5,6,'hijau','2021-09-25 13:16:26','2021-09-25 13:16:26'),(6,6,'merah','2021-09-25 13:16:26','2021-09-25 13:16:26'),(7,7,'hijau','2021-09-25 13:16:33','2021-09-25 13:16:33'),(8,7,'merah','2021-09-25 13:16:33','2021-09-25 13:16:33'),(9,8,'hijau','2021-09-25 13:16:36','2021-09-25 13:16:36'),(10,8,'merah','2021-09-25 13:16:36','2021-09-25 13:16:36'),(11,9,'hijau','2021-09-25 13:16:40','2021-09-25 13:16:40'),(12,9,'merah','2021-09-25 13:16:40','2021-09-25 13:16:40'),(13,10,'hijau','2021-09-25 13:16:43','2021-09-25 13:16:43'),(14,10,'merah','2021-09-25 13:16:43','2021-09-25 13:16:43'),(15,11,'hijau','2021-09-25 13:16:47','2021-09-25 13:16:47'),(16,11,'merah','2021-09-25 13:16:47','2021-09-25 13:16:47'),(27,24,'HIjau','2021-09-25 19:30:26','2021-09-25 19:30:26'),(28,24,'Merah','2021-09-25 19:30:27','2021-09-25 19:30:27'),(29,26,'HIjau','2021-09-25 19:31:53','2021-09-25 19:31:53'),(30,26,'Merah','2021-09-25 19:31:53','2021-09-25 19:31:53');
/*!40000 ALTER TABLE `product_warna` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin@mnc.com',NULL,'$2y$10$AnwAmlF4E0aEy5mAxKH16.uVi4pmYm37ZcLp/v.IHEcU5sTyg13.2','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzI1Nzg4MDMwMDAsIm5iZiI6MTYzMjU3ODc0MywiaWF0IjoxNjMyNTc4NzMzLCJqdGkiOiJlaEZKVmNwb1plbGFHa3o0MnBmaWlleHdOSlBnamExTUk0MnBwSTlDd0RnPSIsImRhdGEiOnsiaWQiOjEsImVtYWlsIjoiYWRtaW5AbW5jLmNvbSIsIm5hbWUiOiJhZG1pbiJ9fQ.wHEGsF8cp_3IPLgIVk9uqCHjEOAXQwTgDaZRiJq9-Uw','2021-07-11 07:35:21','2021-09-25 14:05:33');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'mnc'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-26  2:35:13
