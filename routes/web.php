<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Login Route
 */
$router->get('/login', [
    'as' => 'login',
    'uses' => 'LoginController@index',
]);
$router->post('/login', [
    'as' => 'submit-login',
    'uses' => 'LoginController@store',
]);

$router->get('/logout', [
    'as' => 'logout',
    'uses' => 'LogoutController@index',
]);


/**
 * Index Route
 */
$router->get('/', [
    'as' => 'index',
    'uses' => 'IndexController@index',
]);

$router->get('/dashboard', [
    'as' => 'index',
    'uses' => 'IndexController@index',
]);

// $router->get('/category', [
//     'as' => 'getCategory.index',
//     'uses' => 'CategoryController@indexWeb',
// ]);

// $router->get('/category', [
//     'as' => 'getCategory.indexLoad',
//     'uses' => 'CategoryController@indexWebLoad',
// ]);

$router->group(['prefix' => 'product'], function () use ($router) {
    $router->get('/', [
        'as' => 'getProduct.index',
        'uses' => 'ProductController@indexWeb',
    ]);
    $router->get('/product_load', [
        'as' => 'getProduct.Load',
        'uses' => 'ProductController@indexWebLoad',
    ]);
    $router->get('/detail/{id}', [
        'as' => 'getProduct.detail',
        'uses' => 'ProductController@detailWeb',
    ]);
    $router->get('/form', [
        'as' => 'formProduct',
        'uses' => 'ProductController@form',
    ]);
    $router->post('/add', [
        'as' => 'addProduct',
        'uses' => 'ProductController@storeWeb',
    ]);
    $router->post('/view-delete', [
        'as' => 'view-delete-product',
        'uses' => 'ProductController@viewDeleteWeb',
    ]);
    $router->post('/delete', [
        'as' => 'delete-product',
        'uses' => 'ProductController@deleteWeb',
    ]);
});