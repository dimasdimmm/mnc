<?php

namespace App\Model;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use DB;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'email_verified_at',
        'password',
        'api_oken',
        'created_at',
        'updated_at',
    ];

    protected static $customMessages = [
        'email.unique_nocase' => 'Email telah terpakai',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'api_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getUser($id)
    {
        $data = $this->select(
            '*'
        )
        ->where('id', '=', $id)
        ->first();
        return $data;
    }

    public function checkUserByEmail($email, $IDNotIn=null)
    {
        $query = $this->select(
            '*'
        );
        $query->where('username', '=', $username);
        if ($IDNotIn <> null) {
            $query->where('id', '<>', $IDNotIn);
        }
        $data = $query->first();
        return $data;
    }

    public function getByID($id)
    {
        $data = $this::find($id);
        return $data;
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
    
}
