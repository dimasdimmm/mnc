<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProductUkuran extends Model
{
    protected $table = 'product_ukuran';
    protected $fillable = [
        'product_id',
        'name',
        'price',
        'created_at',
        'updated_at',
    ];

    public function get()
    {
        $data = $this->select(
            '*'
        )
        ->get();
        return $data;
    }

    public function getByID($id)
    {
        $data = $this::find($id);
        return $data;
    }
    
}