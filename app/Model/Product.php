<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Model\ProductGambar;
use App\Model\ProductWarna;
use App\Model\ProductUkuran;
use App\Model\Category;

class Product extends Model
{
    protected $table = 'product';
    protected $fillable = [
        'code', 
        'category_id',
        'name',
        'desc',
        'base_price',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
    ];

    public function get()
    {
        $data = $this->select(
            '*'
        )
        ->get();
        return $data;
    }

    public function getByID($id)
    {
        $data = $this::find($id);
        return $data;
    }

    public function productWarna()
    {
        return $this->hasMany(ProductWarna::class,'product_id', 'id');
    }

    public function productGambar()
    {
        return $this->hasMany(ProductGambar::class,'product_id', 'id');
    }

    public function productUkuran()
    {
        return $this->hasMany(ProductUkuran::class,'product_id', 'id');
    }

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    public function checkByCode($code, $IDNotIn=null)
    {
        $query = $this->select(
            '*'
        );
        $query->where('code', '=', $code);
        if ($IDNotIn <> null) {
            $query->where('id', '<>', $IDNotIn);
        }
        $data = $query->first();
        return $data;
    }

    public function getTotalData($requestData=[])
    {
        // DB::enableQueryLog(); // Enable query log
        $query = $this->select(DB::raw('count(1) as total_data'));
        $query->leftJoin('category', 'product.category_id', '=' ,'category.id');
        $query->whereRaw('1=?', 1);
        if ($requestData['search'] != null) {   // if there is a search parameter, $requestData['search']
            $query->where('product.name', 'like', '%'.$requestData['search'].'%');
        }
        if ($requestData['category_id'] != null) {   // if there is a search parameter, $requestData['category_id']
            $query->where('product.category_id', '=', $requestData['category_id']);
        }
        $data = $query->first();
        // dd(DB::getQueryLog());
        return $data->total_data;
    }

    public function getListData($requestData=[])
    {
        // DB::enableQueryLog(); // Enable query log
        
        $query = $this::addSelect([
            'product.id',
            'product.code', 
            'product.category_id',
            'product.name',
            'product.desc',
            'product.base_price',
            'product.created_at',
            'product.updated_at',
            'product.created_by',
            'product.updated_by',
            'category.name as category_name',
        ]);
        $query->leftJoin('category', 'product.category_id', '=' ,'category.id');
        $query->whereRaw('1=?', 1);
        if ($requestData['search'] != null) {   // if there is a search parameter, $requestData['search']
            $query->whereRaw('LOWER(product.name) LIKE ? ',['%'.trim(strtolower($requestData['search'])).'%']);
        }
        if ($requestData['category_id'] != null) {   // if there is a search parameter, $requestData['category_id']
            $query->where('product.category_id', '=', $requestData['category_id']);
        }

        $query->orderBy('product.created_at', 'desc');

        if ($requestData['search'] == '' || $requestData['category_id'] == null ) {
            $query->limit($requestData['limit']);
            $query->offset($requestData['offset']);
        }
        $query->get();
        // dd(DB::getQueryLog());
        $data = $query->get();
        return $data;
    }
}