<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Category extends Model
{
    protected $table = 'category';
    protected $fillable = [
        'code',
        'name',
        'desc',
        'created_at',
        'updated_at',
    ];

    public function get()
    {
        $data = $this->select(
            '*'
        )
        ->get();
        return $data;
    }

    public function getByID($id)
    {
        $data = $this::find($id);
        return $data;
    }

    public function checkByCode($code, $IDNotIn=null)
    {
        $query = $this->select(
            '*'
        );
        $query->where('code', '=', $code);
        if ($IDNotIn <> null) {
            $query->where('id', '<>', $IDNotIn);
        }
        $data = $query->first();
        return $data;
    }

    public function getTotalData($requestData=[])
    {
        // DB::enableQueryLog(); // Enable query log
        $query = $this->select(DB::raw('count(1) as total_data'));
        $query->whereRaw('1=?', 1);
        if (!empty($requestData['search'])) {   // if there is a search parameter, $requestData['search']
            $query->whereRaw('lower(name) like ?', ['%'.strtolower($requestData['search']).'%']);
        }
        if (isset($requestData['start_date'])) {
            $date  = explode("/", $requestData['start_date']);
            $date_fix = $date[2]."-".$date[0]."-".$date[1];
            $query->where('updated_at', '>=', ''.$date_fix.'');
        }
        if (isset($requestData['end_date'])) {
            $date  = explode("/", $requestData['end_date']);
            $date_fix = $date[2]."-".$date[0]."-".$date[1];
            $query->where('created_at', '<=', ''.$date_fix.'');
        }
        $data = $query->first();
        // dd(DB::getQueryLog());
        return $data->total_data;
    }

    public function getListData($requestData=[])
    {
        // DB::enableQueryLog(); // Enable query log
        $query = $this::addSelect([
            'id',
            'code',
            'name',
            'desc',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ]);
        $query->whereRaw('1=?', 1);
        if (!empty($requestData['search'])) {   // if there is a search parameter, $requestData['search']
            $query->whereRaw('lower(name) like ?', ['%'.strtolower($requestData['search']).'%']);
        }
        if (isset($requestData['start_date'])) {
            $date  = explode("/", $requestData['start_date']);
            $date_fix = $date[2]."-".$date[0]."-".$datecategories[1];
            $query->where('updated_at', '>=', ''.$date_fix.'');
        }
        if (isset($requestData['end_date'])) {
            $date  = explode("/", $requestData['end_date']);
            $date_fix = $date[2]."-".$date[0]."-".$date[1];
            $query->where('updated_at', '<=', ''.$date_fix.'');
        }
        $query->orderBy($requestData['column_order_by'], $requestData['order_dir'])
        ->limit($requestData['limit'])
        ->offset($requestData['offset'])
        ->get();
        // dd(DB::getQueryLog());
        $data = $query->get();
        return $data;
    }
}