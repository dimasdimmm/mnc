<?php

namespace App\Http\Controllers;

use Illuminate\Routing\UrlGenerator;
use Illuminate\Http\Request;
use App\Model\Category;
use Auth;
use Cache;

class CategoryController extends BaseController
{
    
    public function index(Request $req)
    {
        $key = 'category';
        $ttl = 30 * 180;
		$response = Cache::remember($key, $ttl, function() {
            $data = (new Category())->get();
            return response()->json([
                "status" => true,
                "message" => "Sukses Get Data",
                "data" => $data,
            ], 200);
        });

        return $response;
    }
}