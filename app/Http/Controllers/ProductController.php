<?php

namespace App\Http\Controllers;

use Illuminate\Routing\UrlGenerator;
use Illuminate\Http\Request;
use App\Model\Product;
use App\Model\ProductGambar;
use App\Model\ProductUkuran;
use App\Model\ProductWarna;
use App\Lib\Service\StoreProduct;
use App\Lib\Service\CallApi;
use Auth;
use Cache;
use Validator;
use DB;

class ProductController extends BaseController
{
    public function store(Request $req)
    {
        $date = date('Y-m-d H:i:s');
        $userToken = $this->getUserToken($req);
        $request = $req->toArray();
        $rules = [
            'code' => 'required', //Must be a number and length of value is 8
            'category_id' => 'required',
            'name' => 'required',
            'desc' => 'required',
            'base_price' => 'required',
            'gambar' => 'required|array',
            'warna' => 'required|array',
            'ukuran' => 'required|array',
        ];

        $validator = Validator::make($request, $rules);
        if (!$validator->passes()) {
            $message = $validator->errors()->all();
            return response()->json(
                [
                    "status" => false,
                    "message" => $message,
                    "data" => [],
                ], 
            400);
        } 
        $request['created_by'] = $userToken->id;
        $request['created_at'] = $date;

        list($error, $product) = (new StoreProduct($request, null, 'add'))->run();

        if (!empty($error)) {
            return response()->json([
                "status" => false,
                "message" => $error,
                "data" => $product,
            ], 400);
        }

        return response()->json([
            "status" => true,
            "message" => "Sukses Insert",
            "data" => $product,
        ], 200);
    } 

    public function index(Request $req)
    {
        $limit = isset($req->limit) ? $req->limit : 10;
        $page = isset($req->page) ? $req->page : 1;
        $pageStart = (($page-1) * $limit);
        
        $requestData = [
            'limit' => $limit,
            'offset' => $pageStart,
            'page' => $page,
            'search' => $req->search,
            'category_id' => $req->category_id,
        ];

        $data = (new Product())->getListData($requestData);
        $totalDataAll = (new Product())->getTotalData($requestData);
        
        return response()->json([
            "status" => true,
            "message" => "Sukses Get Data",
            "total" => $totalDataAll,
            "data" => $data,
        ], 200);
    }

    public function detail(Request $req, $id)
    {
        $key = 'product_'.$id;
        $ttl = 30 * 180;
		$response = Cache::remember($key, $ttl, function() use($id) {
            $data = (new Product())->getByID($id);
            $category = $data->category;
            $productGambar = $data->productGambar;
            $productWarna = $data->productWarna;
            $productUkuran = $data->productUkuran;
            return response()->json([
                "status" => true,
                "message" => "Sukses Get Data",
                "data" => [
                    "product" => $data,
                ],
            ], 200);
        });

        return $response;
    }

    public function delete($id)
    {
        $cacheKey = 'product_'.$id;
        DB::beginTransaction();
        try {
            Product::where('id', $id)->delete();
            ProductGambar::where('product_id', $id)->delete();
            ProductWarna::where('product_id', $id)->delete();
            ProductUkuran::where('product_id', $id)->delete();
            Cache::forget($cacheKey);
            DB::Commit();
        } catch(Exception $e) {
            DB::Rollback();
            return response()->json([
                "status" => false,
                "message" => $e->getMessage(),
                "data" => [
                    "product" => $data,
                ],
            ], 400);    
        }
        return response()->json([
            "status" => true,
            "message" => "Sukses Delete Data",
            "data" => [],
        ], 200);
        
    }

    /**Cms */

    public function indexWeb(Request $req)
    {
        /** set base uri*/
        $currentUrl = url()->current();
        $baseUrl = $this->baseUrl;
        $dataSession = $req->session()->all();
        $category = $this->getCategory($dataSession);

        return view(
            'product.index',
            compact(
                'currentUrl',
                'baseUrl',
                'dataSession',
                'category'
            )
        );
    }

    protected function getCategory($dataSession)
    {
        $url = route('api.getCategory');        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type:application/json",
            "Authorization: Bearer ".$dataSession['data']['token'].""
        )); 
        $response = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return json_decode($response, true);
    }
    
    public function indexWebLoad(Request $req)
    {
        /** set base uri*/
        $currentUrl = url()->current();
        $baseUrl = $this->baseUrl;
        $dataSession = $req->session()->all();
        $totalData = $this->getTotalData($req);
        $perPage		= 10;
        $pageNum		= (isset($req->page_num)) ? max(1, $req->page_num) : 1;
        $totalPage = ceil($totalData/$perPage);
        $pageNum = ($pageNum > $totalPage) ? $totalPage : $pageNum;
        if ($pageNum == 0) {
            $pageNum = 1;
        }
        $pageStart = (($pageNum-1) * $perPage);
        
        $requestData = [
            'limit' => $perPage,
            'offset' => $pageStart,
            'page' => $pageNum,
            'search' => $req->search,
            'category_id' => $req->category_id,
        ];

        $dataProduct = $this->getListDataWeb($requestData, $dataSession);
        $data = $dataProduct['data'];
         
        return view(
            'product.index_load',
            compact(
                'baseUrl',
                'data',
                'pageNum',
                'totalPage',
                'pageStart'
            )
        );
    }

    protected function getListDataWeb($requestData=[], $dataSession=[])
    {
        $endpoint = route('api.getProduct');  
        $params = $requestData;
        $url = $endpoint . '?' . http_build_query($params);      
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type:application/json",
            "Authorization: Bearer ".$dataSession['data']['token'].""
        )); 
        $response = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return json_decode($response, true);
    }

    protected function getTotalData($req)
    {
        $data = (new Product())->getTotalData([
            'search' => $req->search,
            'category_id' => $req->category_id,
        ]);
        return $data;
    }

    public function detailWeb(Request $req,$id)
    {
        /** set base uri*/
        $currentUrl = url()->current();
        $baseUrl = $this->baseUrl;
        $dataSession = $req->session()->all();
        $result = $this->getDetaildata($id, $dataSession);  
        $data = $result['data'];
        
        return view(
            'product.detail',
            compact(
                'baseUrl',
                'data',
                'dataSession',
                'currentUrl'
            )
        );
              
    }

    protected function getDetaildata($id, $dataSession)
    {
        $url = route('api.DetailProduct', $id);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type:application/json",
            "Authorization: Bearer ".$dataSession['data']['token'].""
        )); 
        $response = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return json_decode($response, true);
    }

    public function viewDeleteWeb(Request $req)
    {
        $id = $req->id;
        $name = $req->name;
        
        return view('product.delete', compact(
            'id',
            'name'
        ));
    }

    public function deleteWeb(Request $req){
        $dataSession = $req->session()->all();
        $id = $req->id;
        $url = route('api.delProduct', $id);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type:application/json",
            "Authorization: Bearer ".$dataSession['data']['token'].""
        )); 
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
    }

    public function form(Request $req)
    {
        /** set base uri*/
        $currentUrl = url()->current();
        $baseUrl = $this->baseUrl;
        $dataSession = $req->session()->all();
        $category = $this->getCategory($dataSession);
        
        return view(
            'product.form',
            compact(
                'currentUrl',
                'baseUrl',
                'dataSession',
                'category'
            )
        );
    }

    public function storeWeb(Request $req)
    {
        /** set base uri*/
        $request = $req->toArray();
        $dataSession = $req->session()->all();
        $requestFinal = $this->validateReq($request);
        $dataJson = json_encode($requestFinal);
        $url = route('api.storeProduct');
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataJson);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type:application/json",
            "Authorization: Bearer ".$dataSession['data']['token'].""
        )); 
        $response = json_decode(curl_exec($ch), true);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($httpcode == 200) {
            return redirect()->route('getProduct.detail', $response['data']['id'])->with('message', 'Data Berhasil Disimpan!');
        } else {
            return redirect()->back()->with('message', 'Data Gagal Disimpan :'.$response['message']);
        }
    }

    protected function validateReq($request)
    {
        unset($request['_token']);
        if (isset($request['id']) || $request['id'] == null) {
            unset($request['id']);
        }
        //generate field product ukuran
        $totalUkuran = count($request['ukuran']);
        for ($i=0; $i < $totalUkuran; $i++) {
            $request['ukuranFinal'][$i]['size'] = $request['ukuran'][$i];
            $request['ukuranFinal'][$i]['harga'] = $request['harga'][$i];
        }
        if (isset($request['ukuran'])) {
            unset($request['ukuran']);
        }
        if (isset($request['harga'])) {
            unset($request['harga']);
        }
        $request['ukuran']= $request['ukuranFinal'];
        if (isset($request['ukuranFinal'])) {
            unset($request['ukuranFinal']);
        }

        return $request;
    }

}