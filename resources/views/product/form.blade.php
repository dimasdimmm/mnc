@extends('layout.app',[

])
@section('content')
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container">
        <!--begin::Card-->
        <div class="card card-custom card-shadowless rounded-top-0">
            <!--begin::Body-->
            <div class="card-body p-0">
                <div class="row justify-content-center py-8 px-8 py-lg-15 px-lg-10">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <button class="close" data-dismiss="alert"><i class="fa fa-close"></i></button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if($errors->any())
                        {!! implode('', $errors->all('<h2 style="color:red">:message</h2>')) !!}
                    @endif
                    <div class="col-xl-12 col-xxl-10">
                        <a href="{{ route('getProduct.index') }}" class="btn btn-success font-weight-bolder" data-wizard-type="action-submit" style="text-align:left">Kembali</a>
                        <!--begin::Wizard Form-->
                        <form class="form" id="kt_form" method="POST" action="{{ route('addProduct') }}">
                            @csrf
                            <div class="row justify-content-center">
                                <div class="col-xl-9">
                                    <!--begin::Wizard Step 1-->
                                    <div class="my-5 step" data-wizard-type="step-content" data-wizard-state="current">
                                        <h5 class="text-dark font-weight-bold mb-10">Form Product</h5>
                                        <!--begin::Group-->
                                        <div class="form-group row">
                                            <label class="col-form-label col-xl-3 col-lg-3">Unit<span class="text-danger">*</span></label>
                                            <div class="col-xl-9 col-lg-9">
                                                <select class="form-control form-control-lg form-control-solid" name="category_id" id="category_id" required>
                                                    <option value="">Pilih ...</option>
                                                    @if (!empty($category['data']))
                                                        @foreach ($category['data'] as $item)
                                                            <option value="{{ $item['id'] }}" {{ isset($data->category_id) && $data->category_id == $item['id'] ? 'selected' : '' }}>{{ $item['name'] }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <!--end::Group-->
                                        <!--begin::Group-->
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Kode</label>
                                            <div class="col-lg-9 col-xl-9">
                                                <input class="form-control form-control-solid form-control-lg" name="code" id="code" type="text" value="{{ isset($data->code) ? $data->code : old('code') }}" />
                                            </div>
                                        </div>
                                        <!--end::Group-->
                                        <!--begin::Group-->
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Nama<span class="text-danger">*</span></label>
                                            <div class="col-lg-9 col-xl-9">
                                                <input class="form-control form-control-solid form-control-lg" name="name" id="name" type="text" value="{{ isset($data->name) ? $data->name : old('name') }}" required />
                                            </div>
                                        </div> 
                                        <!--end::Group-->
                                        <!--begin::Group-->
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Deskripsi</label>
                                            <div class="col-lg-9 col-xl-9">
                                                <input class="form-control form-control-solid form-control-lg" name="desc" id="desc" type="text" value="{{ isset($data->desc) ? $data->desc : old('desc') }}" />
                                            </div>
                                        </div>
                                        <!--end::Group-->
                                        <!--begin::Group-->
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Base Price</label>
                                            <div class="col-lg-9 col-xl-9">
                                                <input class="form-control form-control-solid form-control-lg" name="base_price" id="base_price" type="text" value="{{ isset($data->base_price) ? $data->base_price : old('base_price') }}" />
                                            </div>
                                        </div>
                                        <!--end::Group-->
                                    </div>
                                    <br>
                                    <div class="my-5 step" data-wizard-type="step-content" data-wizard-state="current">
                                        <h5 class="text-dark font-weight-bold mb-10">Form Product Gambar</h5>
                                        <!--begin::Group-->
                                        <div class="form-group row">
                                            <div class="col-lg-9 col-xl-9 input_fields_wrap_gambar">
                                                <input class="form-control form-control-solid form-control-lg" name="gambar[]" id="gambar" type="text" placeholder="gambar" /><br>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-9 col-xl-9">
                                                <button type="button" id="add_more_product_gambar" class="btn btn-success">Add More</button>
                                            </div>
                                        </div>
                                        <!--end::Group-->
                                    </div>
                                    <br>
                                    <div class="my-5 step" data-wizard-type="step-content" data-wizard-state="current">
                                        <h5 class="text-dark font-weight-bold mb-10">Form Product Warna</h5>
                                        <!--begin::Group-->
                                        <div class="form-group row">
                                            <div class="col-lg-9 col-xl-9 input_fields_wrap_warna">
                                                <input class="form-control form-control-solid form-control-lg" name="warna[]" id="warna" type="text" placeholder="warna" /><br>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-9 col-xl-9">
                                                <button type="button" id="add_more_product_warna" class="btn btn-success">Add More</button>
                                            </div>
                                        </div>
                                        <!--end::Group-->
                                    </div>
                                    <br>
                                    <div class="my-5 step" data-wizard-type="step-content" data-wizard-state="current">
                                        <h5 class="text-dark font-weight-bold mb-10">Form Product Ukuran</h5>
                                        <!--begin::Group-->
                                        <div class="form-group row">
                                            <div class="col-lg-9 col-xl-9 input_fields_wrap_ukuran">
                                                <input class="form-control form-control-solid form-control-lg" name="ukuran[]" id="ukuran" type="text" placeholder="ukuran" /><br>
                                                <input class="form-control form-control-solid form-control-lg" name="harga[]" id="harga" type="text" placeholder="harga" /><br><br>
                                            </div>
                                        </div>
                                        <div id="load_form_product_ukuran"></div>
                                        <div class="form-group row">
                                            <div class="col-lg-9 col-xl-9">
                                                <button type="button" id="add_more_product_ukuran" class="btn btn-success">Add More</button>
                                            </div>
                                        </div>
                                        <!--end::Group-->
                                    </div>
                                    <!--begin::Wizard Actions-->
                                    <input type="hidden" name="id" id="id" value="{{ isset($data->id) ?$data->id : null }}" >
                                    <div class="d-flex justify-content-between border-top pt-10 mt-15">
                                        <div>
                                            <button type="submit" class="btn btn-success font-weight-bolder px-9 py-4" data-wizard-type="action-submit">Simpan</button>
                                        </div>
                                    </div>
                                    <!--end::Wizard Actions-->
                                </div>
                            </div>
                        </form>
                        <!--end::Wizard Form-->
                    </div>
                </div>
            </div>
            <!--end::Body-->
        </div>
        <!--end::Card-->
    </div>
</div>
<script src="{{ asset('assets/jquery/jquery-3.6.1.min.js') }}"></script>
<script src="{{ asset('assets/js/select2/select2.min.js') }}"></script>
<script>
    $(document).ready(function(){
        var max_fields_gambar     = 10; //maximum input boxes allowed
        var wrapper_gambar   	  = $(".input_fields_wrap_gambar"); //Fields wrapper
        var add_button_gambar     = $("#add_more_product_gambar"); //Add button ID        
        var x_gambar = 1; //initlal text box count

        var max_fields_warna      = 10; //maximum input boxes allowed
        var wrapper_warna   	  = $(".input_fields_wrap_warna"); //Fields wrapper
        var add_button_warna      = $("#add_more_product_warna"); //Add button ID        
        var x_warna = 1; //initlal text box count

        var max_fields_ukuran     = 10; //maximum input boxes allowed
        var wrapper_ukuran   	  = $(".input_fields_wrap_ukuran"); //Fields wrapper
        var add_button_ukuran     = $("#add_more_product_ukuran"); //Add button ID        
        var x_ukuran = 1; //initlal text box count

        $(add_button_gambar).click(function(e){ //on add input button click
            e.preventDefault();
            if(x_gambar < max_fields_gambar){ //max input box allowed
                x_gambar++; //text box increment
                $(wrapper_gambar).append('<div><input class="form-control form-control-solid form-control-lg" name="gambar[]" id="gambar" type="text" placeholder="gambar" /><a href="#" class="remove_field_gambar">Remove</a></div><br>'); //add input box
            }
        });
        
        $(wrapper_gambar).on("click",".remove_field_gambar", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x_gambar--;
        });

        $(add_button_warna).click(function(e){ //on add input button click
            e.preventDefault();
            if(x_warna < max_fields_warna){ //max input box allowed
                x_warna++; //text box increment
                $(wrapper_warna).append('<div><input class="form-control form-control-solid form-control-lg" name="warna[]" id="warna" type="text" placeholder="warna" /><a href="#" class="remove_field_warna">Remove</a></div><br>'); //add input box
            }
        });
        
        $(wrapper_warna).on("click",".remove_field_warna", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x_warna--;
        });

        $(add_button_ukuran).click(function(e){ //on add input button click
            e.preventDefault();
            if(x_ukuran < max_fields_ukuran){ //max input box allowed
                x_ukuran++; //text box increment
                $(wrapper_ukuran).append('<div><input class="form-control form-control-solid form-control-lg" name="ukuran[]" id="ukuran" type="text" placeholder="ukuran" /><br><input class="form-control form-control-solid form-control-lg" name="harga[]" id="harga" type="text" placeholder="harga" /><br><a href="#" class="remove_field_ukuran">Remove</a></div><br>'); //add input box
            }
        });
        
        $(wrapper_ukuran).on("click",".remove_field_ukuran", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x_ukuran--;
        });
    });
</script>
@endsection